package agh.heart.observers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;

import agh.heart.HeaRTService;


public abstract class Observer extends BroadcastReceiver {
    FirebaseJobDispatcher dispatcher = null;

    @Override
    public void onReceive(Context context, Intent intent) {
        setupDispatcher(context);
        dispatcher.schedule(prepareJob());
        Log.d("Observer", "job scheduled");
    }

    private Job prepareJob() {
        return dispatcher.newJobBuilder()
                .setService(HeaRTService.class)
                .setTag("HeaRTService")
                .setLifetime(Lifetime.UNTIL_NEXT_BOOT) // will not trigger after phone reboot
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                .setReplaceCurrent(true) // will trigger once for many schedules
                .build();
    }

    private void setupDispatcher(Context context) {
        if (dispatcher == null) {
            dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
        }
    }

    public abstract void register(Context context);

    public void unregister(Context context) {
        context.unregisterReceiver(this);
    }
}
