package agh.heart.observers;

import android.content.Context;
import android.content.IntentFilter;

import com.aware.Locations;


public class Location extends Observer {

    @Override
    public void register(Context context) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Locations.ACTION_AWARE_LOCATIONS);
        context.registerReceiver(this, intentFilter);
    }
}
